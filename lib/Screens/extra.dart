// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:video_player/video_player.dart';

// class FormScreen extends StatefulWidget {
//   @override
//   _FormScreenState createState() => _FormScreenState();
// }

// class _FormScreenState extends State<FormScreen> {
//   File _image;
//   File _video;

//   VideoPlayerController videoPlayerController;

//   final picker = ImagePicker();

//   Future getImage() async {
//     final pickedImage = await picker.getImage(source: ImageSource.gallery);
//     setState(() {
//       if (pickedImage != null) {
//         _image = File(pickedImage.path);
//       } else {
//         print("No image Selected");
//       }
//     });
//   }

//   Future getVideo() async {
//     final video = await picker.getVideo(source: ImageSource.gallery);
//     _video = File(video.path);

//     videoPlayerController = VideoPlayerController.file(_video)
//       ..initialize().then((_) {
//         setState(() {});
//         videoPlayerController.play();
//       });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Container(
//         height: double.infinity,
//         width: double.infinity,
//         color: Colors.blueGrey[100],
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               //Clipper
//               Stack(
//                 children: [
//                   Transform.rotate(
//                     angle: -0.3,
//                     child: ClipPath(
//                       clipper: WaveClipperTwo(flip: true),
//                       child: Container(
//                         height: 130,
//                         color: Colors.blue[300],
//                       ),
//                     ),
//                   ),
//                   ClipPath(
//                     clipper: WaveClipperTwo(flip: true),
//                     child: Container(
//                       height: 100,
//                       color: Colors.blue[500],
//                     ),
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 20,
//               ),

//               Container(
//                 padding: EdgeInsets.symmetric(horizontal: 40.0),
//                 child: Column(
//                   children: [
//                     TextField(
//                       decoration: InputDecoration(
//                           hintText: "Name",
//                           hintStyle: TextStyle(color: Colors.blue[800])),
//                     ),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     TextField(
//                       decoration: InputDecoration(
//                           hintText: "Age",
//                           hintStyle: TextStyle(color: Colors.blue[800])),
//                     ),
//                     Row(
//                       children: [
//                         Text(
//                           "Agree with ",
//                           style: TextStyle(
//                             color: Colors.blue[800],
//                           ),
//                         ),
//                         MaterialButton(
//                           onPressed: () {},
//                           child: Text(
//                             "Terms and Conditions",
//                             style: TextStyle(
//                               color: Colors.blue[800],
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                     Stack(
//                       children: [
//                         Container(
//                           child: _image == null
//                               ? Text("No Image")
//                               : Image.file(_image),
//                         ),
//                         CloseButton(
//                           color: Colors.red,
//                         )
//                       ],
//                     ),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         MaterialButton(
//                           color: Colors.blue,
//                           onPressed: () {
//                             getImage();
//                           },
//                           child: Text("Select Image"),
//                         ),
//                         SizedBox(
//                           width: 30,
//                         ),
//                       ],
//                     ),
//                     Column(
//                       children: [
//                         if (_video != null)
//                           videoPlayerController.value.isInitialized
//                               ? AspectRatio(
//                                   aspectRatio:
//                                       videoPlayerController.value.aspectRatio,
//                                   child: VideoPlayer(videoPlayerController),
//                                 )
//                               : Container()
//                         else
//                           Text("Click on select video button to select video"),
//                         MaterialButton(
//                           color: Colors.green,
//                           onPressed: () {
//                             getVideo();
//                           },
//                           child: Text("Select Video"),
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
