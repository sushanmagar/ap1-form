// import 'dart:io';

// import 'package:ap1_prject/Screens/Custom/customTextfield.dart';
// import 'package:flutter/material.dart';
// import 'package:dropdown_formfield/dropdown_formfield.dart';
// import 'package:image_picker/image_picker.dart';

// class FormScreen extends StatefulWidget {
//   @override
//   _FormScreenState createState() => _FormScreenState();
// }

// class _FormScreenState extends State<FormScreen> {
//   List<DropdownMenuItem> items = [];

//   String _chosenGame;

//   bool value = false;

//   //For picking up image
//   File _image;
//   File videofile;

//   String path;

//   final picker = ImagePicker();

//   Future getImage() async {
//     final pickedImage = await picker.getImage(source: ImageSource.gallery);
//     setState(() {
//       if (pickedImage != null) {
//         _image = File(pickedImage.path);
//       } else {
//         print("No image Selected");
//       }
//     });
//   }

//   getVideo() async {
//     PickedFile video = await picker.getVideo(source: ImageSource.gallery);
//     if (video != null) {
//       setState(() {
//         path = video.path;
//         videofile = File(path);
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     var _width = MediaQuery.of(context).size.width;
//     var _height = MediaQuery.of(context).size.height;

//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         title: Text(
//           "Application Form",
//           style: TextStyle(color: Color(0xff692873)),
//         ),
//         leading: Icon(Icons.arrow_back, color: Colors.grey[500]),
//         elevation: 0,
//       ),
//       body: Container(
//         padding: EdgeInsets.symmetric(horizontal: 30),
//         height: _height,
//         width: _width,
//         child: SingleChildScrollView(
//           child: Column(
//             children: <Widget>[
//               Container(
//                 height: _height / 4,
//                 child: Center(
//                   child: InkWell(
//                     onTap: getImage,
//                     child: CircleAvatar(
//                       radius: 75,
//                       backgroundColor: Color(0xff692873),
//                       child: CircleAvatar(
//                         backgroundColor: Colors.white,
//                         radius: 70,
//                         child: ClipOval(
//                           child: _image == null
//                               ? Icon(Icons.camera_roll_rounded)
//                               : Image.file(
//                                   _image,
//                                   width: double.infinity,
//                                   height: double.infinity,
//                                   fit: BoxFit.fill,
//                                 ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//               Container(
//                 child: Column(
//                   children: [
//                     Container(
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Container(
//                             color: Colors.white10,
//                             child: DropDownFormField(
//                               titleText: "Audition Location",
//                               hintText: "Choose a location",
//                               value: _chosenGame,
//                               onSaved: (value) {
//                                 setState(() {
//                                   _chosenGame = value;
//                                 });
//                               },
//                               onChanged: (value) {
//                                 setState(() {
//                                   _chosenGame = value;
//                                 });
//                               },
//                               dataSource: [
//                                 {
//                                   "display": "Bhrikutimandap, Kathmandu",
//                                   "value": "Bhrikutimandap, Kathmandu",
//                                 },
//                                 {
//                                   "display": "Biratnagar",
//                                   "value": "Biratnagar",
//                                 },
//                                 {
//                                   "display": "Hetauda",
//                                   "value": "Hetauda",
//                                 },
//                                 {
//                                   "display": "Pokhara",
//                                   "value": "Pokhara",
//                                 },
//                                 {
//                                   "display": "Jhapa",
//                                   "value": "Jhapa",
//                                 },
//                               ],
//                               textField: 'display',
//                               valueField: 'value',
//                             ),
//                           ),
//                           SizedBox(height: 18),
//                           // TextFormField(
//                           //   decoration: InputDecoration(
//                           //     suffixIcon: InkWell(
//                           //       onTap: () {
//                           //         print("Dropdown button tapped");
//                           //       },
//                           //       child: Icon(
//                           //         Icons.arrow_drop_down,
//                           //       ),
//                           //     ),
//                           //     border: OutlineInputBorder(),
//                           //   ),
//                           // ),

//                           CustomTextField("Audition Date", "April 25,2021"),
//                           CustomTextField("Full Name", "Sanjay Thapa"),
//                           CustomTextField("DOB", "MM/DD/YYYY"),
//                           CustomTextField("Phone No.", "+977 9876543210"),
//                           CustomTextField("Alt. Phone No.", "+977 9876543210"),
//                           CustomTextField("Email Id", "xyz@gmail.com"),
//                           CustomTextField("Address", "Province6"),
//                           CustomTextField("District", "Kathmandu"),
//                           CustomTextField("Municipality", "Address"),
//                           Column(
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Container(
//                                   padding: EdgeInsets.only(left: 8),
//                                   child: Text(
//                                     "Upload Video",
//                                     style:
//                                         TextStyle(fontWeight: FontWeight.bold),
//                                   ),
//                                 ),
//                                 Stack(
//                                   children: [
//                                     Container(
//                                       child: TextField(
//                                         enabled: false,
//                                         decoration: InputDecoration(
//                                           hintText: path == null
//                                               ? "Video Path"
//                                               : path,
//                                           contentPadding:
//                                               EdgeInsets.only(left: 8.0),
//                                         ),
//                                       ),
//                                     ),
//                                     Positioned(
//                                       left: 212,
//                                       top: 3,
//                                       child: Container(
//                                         decoration: BoxDecoration(
//                                           borderRadius:
//                                               BorderRadius.circular(4),
//                                           gradient: LinearGradient(
//                                             begin: Alignment.centerLeft,
//                                             end: Alignment.centerRight,
//                                             colors: [
//                                               Color(0xffE12C7D),
//                                               Color(0xff692873),
//                                             ],
//                                           ),
//                                         ),
//                                         height: 45,
//                                         width: 120,
//                                         child: MaterialButton(
//                                           onPressed: () {
//                                             getVideo();
//                                           },
//                                           child: Text("Upload"),
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ]),
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//               Container(
//                 height: _height / 4.5,
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     //Row with CheckBox and RichText
//                     Row(
//                       children: [
//                         //Wrapp checkbox inside container to edit border color
//                         Theme(
//                           data: ThemeData(
//                               unselectedWidgetColor: Color(0xff692873)),
//                           child: Transform.scale(
//                             scale: 1.0,
//                             child: Checkbox(
//                                 value: value,
//                                 onChanged: (value) {
//                                   setState(() {
//                                     this.value = value;
//                                   });
//                                 }),
//                           ),
//                         ),
//                         SizedBox(
//                           width: 2,
//                         ),
//                         Expanded(
//                           child: RichText(
//                             text: TextSpan(
//                               children: [
//                                 TextSpan(
//                                   text: "By signing up, you agree to our ",
//                                   style: TextStyle(
//                                     color: Colors.black,
//                                     fontSize: 12,
//                                   ),
//                                 ),
//                                 TextSpan(
//                                   text: "Terms & Conditions ",
//                                   style: TextStyle(
//                                     color: Colors.blue,
//                                     fontSize: 12,
//                                   ),
//                                 ),
//                                 TextSpan(
//                                   text: "and ",
//                                   style: TextStyle(
//                                     color: Colors.black,
//                                     fontSize: 12,
//                                   ),
//                                 ),
//                                 TextSpan(
//                                   text: "Privacy Policy ",
//                                   style: TextStyle(
//                                     color: Colors.blue,
//                                     fontSize: 12,
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                     //Save Button
//                     Container(
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(4),
//                         gradient: LinearGradient(
//                           begin: Alignment.centerLeft,
//                           end: Alignment.centerRight,
//                           colors: [
//                             Color(0xffE12C7D),
//                             Color(0xff692873),
//                           ],
//                         ),
//                       ),
//                       height: 45,
//                       width: 160,
//                       child: MaterialButton(
//                         onPressed: () {},
//                         child: Text("Save"),
//                       ),
//                     ),
//                     SizedBox(
//                       height: 2,
//                     ),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
